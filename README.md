## CubeSat Structures
Structures for CubeSats designed by the Silesian Aerospace Technologies science club. Designed to be used in high altitude balloon missions. 

![CubeSat structures](wireframe.png)

## Description
There are two version available:
 - 1U: 100 x 100 x 113.5mm
 - 2U: 100 x 100 x 227mm

 Structures are designed to be 3D printed on an FDM printer. When printing make sure that perimeters are set to at least 3 and infil value to at least 50%. Use PETG or ABS.
 
 3MF file format was used to allow for easy printing with any slicing software as well as editing with CAD software. Additionally, F3D files are available with the assembly of each structure.

 ## Assembly
 Structure can be assembled according to the figure.
 ![CubeSat structures](assembly.png)


## License
This project is licensed under the terms of the MIT license.

